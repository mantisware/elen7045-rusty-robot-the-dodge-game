# README #

### Rusty Robot (The Dodge Game) ###

A Game of Dodge consists of:
• A single player who can only move left and right and is controlled via the keyboard.
• Raindrops which appear in random positions in the top quarter of the screen and fall
vertically downwards. The number of raindrops on the screen at any one time remains
constant — whenever a raindrop disappears off the bottom of the screen, a new one
appears at the top of the screen.
The objective of the game is to move the player so that he or she does not get wet, that is, the
player must dodge the raindrops. If a raindrop touches the player then the game is over.

### How do I get set up? ###

The project was built in Visual Studio Community Edition, open the solution file (*.sln) in Visual Studio and run.

### Contribution guidelines ###

Project was made for (Elen7045 - Software Development Methodologies, Analysis and Design University of the Witwatersrand)


### Owner ###

Waldo Marais