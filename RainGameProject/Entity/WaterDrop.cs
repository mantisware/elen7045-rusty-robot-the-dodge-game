
using System;
using System.Windows.Forms;

namespace RainGameProject
{
	/// <summary>
	/// Description of WaterDrop.
	/// </summary>
	public class WaterDrop
	{
		private int x;
		
		private int y;
		
		private bool isActive;
		
		private Label dropLabel;
		private int maxY;
		private int minY;
		private int addY;
		
		public WaterDrop(Form gameForm, int dropCount)
		{
			Label CloudsWaterDrop = new Label();
			#region Create Drop
			CloudsWaterDrop.BackColor = System.Drawing.Color.Transparent;
			CloudsWaterDrop.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
			CloudsWaterDrop.Image = global::RainGameProject.Properties.Resources.tex_drop;
			CloudsWaterDrop.Location = new System.Drawing.Point(420, 190);
			CloudsWaterDrop.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			CloudsWaterDrop.Name = "lb_CloudsWaterDrop";
			CloudsWaterDrop.Size = new System.Drawing.Size(36, 70);
			CloudsWaterDrop.TabIndex = 8 + dropCount;
			CloudsWaterDrop.Visible = false;
			CloudsWaterDrop.SendToBack();
			#endregion
			gameForm.Controls.Add(CloudsWaterDrop);

			this.dropLabel= CloudsWaterDrop;
			isActive=false;
			this.dropLabel.Visible=false;
		}
		
		/// <summary>
		/// Drop Start
		/// </summary>
		/// <param name="x">Start X</param>
		/// <param name="y">Start Y</param>
		/// <param name="maxY">Max can drop Y</param>
		/// <param name="minY">Min Start Drop</param>
		/// <param name="addY">Increment drop value</param>
		public void Start(int _x,int _y,int _maxY,int _minY,int _addY)
		{
			if (isActive)
			{
				return;
			}
			x=_x;
			y=_y;
			dropLabel.Top=y;
			dropLabel.Left=x;
			maxY=_maxY;
			minY=_minY;
			addY=_addY;
			isActive=true;
			dropLabel.Visible=true;
		}
		
		/// <summary>
		/// Stop... DUUU!
		/// </summary>
		public void Stop()
		{
			isActive=false;
			dropLabel.Visible=false;
			
		}
		
		/// <summary>
		/// Using the Drop Increment, move the drop.
		/// </summary>
		public void Move()
		{
			if (!isActive)
			{
				return;
			}
			y+=addY;
			if (y>maxY || y<minY)
			{
				dropLabel.Visible=false;
				isActive=false;
				return;
			}
			dropLabel.Top=y;
		}

		public bool IsActive {
			get { return isActive; }
		}

		public int X {
			get { return x; }
		}

		public int Y {
			get { return y; }
		}
	}
}
