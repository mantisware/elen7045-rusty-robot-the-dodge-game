
using System;
using System.Windows.Forms;


namespace RainGameProject
{
	/// <summary>
	/// Description of Clouds.
	/// </summary>
	/// 
	public class Clouds
	{
		private int _x;
		private int _y;
		private Panel _cloudsPanel;
		private Panel _cloudsPanel1;
		private int _addX;
		private int _addY;
		private int _minX;
		private int _maxX;
		private int _minY;
		private int _maxY;
		private int _delay;

		private int _delayCounter;
		private int _leftColumn;
		
		private int _rightColumn;
		private bool _complete;

		private bool _isActive;
		private System.Collections.Generic.List<int> _columnsForRaining;

		private System.Random _random;
		
		public Clouds(Panel cloudsPanel,Panel cloudsPanel1)
		{
			//Setup
			_cloudsPanel=cloudsPanel;
			_cloudsPanel1=cloudsPanel1;
			_cloudsPanel1.SendToBack();
			_cloudsPanel.SendToBack();
			_columnsForRaining=new System.Collections.Generic.List<int>();
			_random=new Random();
			this.Stop();
		}
		
		/// <summary>
		/// Start Config
		/// </summary>
		public void Start(int x,int y,int addX,int addY,int minX,int maxX,int minY,int maxY,int delay)
		{
			_x=x;
			_y=y;
			_addX=addX;
			_addY=addY;
			_minX=minX;
			_maxX=maxX;
			_minY=minY;
			_maxY=maxY;
			_delay=delay;
			_delayCounter=0;
			_cloudsPanel.Left=_x;
			_cloudsPanel.Top=_y;
			_leftColumn=0;
			_rightColumn=10;
			_complete=false;
			_cloudsPanel1.Visible=false;
			_cloudsPanel.Visible=false;
			_cloudsPanel1.SendToBack();
			_cloudsPanel.Visible=true;
			_cloudsPanel1.Visible=false;
			_isActive=true;
		}
		
		
		public void Freeze()
		{
			_isActive=false;	
			
		}

		public void Defreeze()
		{
			_isActive=true;	
			
		}
		
		public void Stop()
		{
			_isActive=false;	
			_cloudsPanel.Visible=false;
			_cloudsPanel1.Visible=false;
		}
		
		/// <summary>
		/// Move Cloud Entity
		/// </summary>
		public void Move()
		{
			if (!_isActive)
			{
				return;
			}

			
			if (_delayCounter<_delay)
			{
				_delayCounter+=1;
				return;
			}
			_delayCounter=0;

			_x+=_addX;
			if (_x<_minX)
			{
				_x=_minX;
			}
			else if (_x>_maxX)
			{
				_x=_maxX;
			}
			
			_cloudsPanel.Left=_x;
			_cloudsPanel.Top=_y;		
			_cloudsPanel1.Left=_x;
			_cloudsPanel1.Top=_y;		
			if (_cloudsPanel1.Visible)
			{
				_cloudsPanel1.SendToBack();
				_cloudsPanel.Visible=true;
				_cloudsPanel1.Visible=false;
				
			}
			else
			{
				_cloudsPanel.SendToBack();
				_cloudsPanel1.Visible=true;
				_cloudsPanel.Visible=false;
				
			}
		}

		#region Get Cloud Positions
		public int BottomX0ForColumn(int column)
		{
			string columnString;
			for (int i=4;i>=0;i=i-1)
			{
				if (column==10)
				{
					columnString="A";
				}
				else
				{
					columnString=column.ToString();
				}
				return this.X0+this._cloudsPanel.Controls["lb_Cloud"+i.ToString()+columnString].Left;			
			}
			return -1;
		}

		public int BottomX1ForColumn(int column)
		{
			string columnString;
			for (int i=4;i>=0;i=i-1)
			{
				if (column==10)
				{
					columnString="A";
				}
				else
				{
					columnString=column.ToString();
				}
				return this.X0+this._cloudsPanel.Controls["lb_Cloud"+i.ToString()+columnString].Left+this._cloudsPanel.Controls["lb_Cloud"+i.ToString()+columnString].Width;					
				
			}
			return -1;
		}

		public int BottomY0ForColumn(int column)
		{
			string columnString;
			for (int i=4;i>=0;i=i-1)
			{
				if (column==10)
				{
					columnString="A";
				}
				else
				{
					columnString=column.ToString();
				}
				return this.Y0+this._cloudsPanel.Controls["lb_Cloud"+i.ToString()+columnString].Top;					
				
			}
			
			return -1;
		}
		
		public int BottomY1ForColumn(int column)
		{
			string columnString;
			for (int i=4;i>=0;i=i-1)
			{
				if (column==10)
				{
					columnString="A";
				}
				else
				{
					columnString=column.ToString();
				}
				return this.Y0+this._cloudsPanel.Controls["lb_Cloud"+i.ToString()+columnString].Top+this._cloudsPanel.Controls["lb_Cloud"+i.ToString()+columnString].Height;
			}
			return -1;
		}
		#endregion

		/// <summary>
		/// Generate rain Drop
		/// </summary>
		public void StartCloudsDrop(WaterDrop drop)
		{
			if (!_isActive)
			{
				return;
			}

			//TODO - Set config
			for(int i = 0; i < 5; i++)
			{
				_columnsForRaining.Add(5);
			}

			int columnIndexer = _random.Next(0, 5);
			int column=_columnsForRaining[columnIndexer];

			for (int i=4;i>=0;i=i-1)
			{
				//Drop it
				drop.Start(this.X0,this.Y0, _cloudsPanel.Parent.Height -70, 0,6);
				break;
			}
			
		}

		#region Entity prop
		public int X0 {
			get {
				return _random.Next(10, _cloudsPanel.Parent.Width - 20);
			}
		}

		public int X1 {
			get {
				return _x + 0;;
			}
		}

		public int Y0 {
			get { return 0;
			}
		}

		public int Y1 {
			get {
				return _y + 0; 
			}
		}

		public int LeftColumn {
			get { return _leftColumn; }
		}
		
		public int RightColumn {
			get { return _rightColumn; }
		}
		
		public bool IsComplete {
			get { return _complete; }
		}
		
		public int Delay {
			get { return _delay; }
			set 
			{ 
				if (value<0)
				{
					_delay = 0;				
				}
				else
				{
					_delay = value;
				}
			}
		}
		#endregion

	}
}
