using System;
using System.Windows.Forms;

namespace RainGameProject
{
	/// <summary>
	/// Description of Player.
	/// </summary>
	public class Player
	{
		private int x;
		
		private int y;
		
		private int minX;
		private int maxX;
		private int addX;
		private Label playerLabel;
		private bool isActive;
		
		/// <summary>
		/// Init Player and makes sure it's stopped
		/// </summary>
		/// <param name="_playerLabel"></param>
		public Player(Label _playerLabel)
		{
			playerLabel = _playerLabel;
			//Force Trans background
			playerLabel.BackColor = System.Drawing.Color.Transparent;
			this.Stop();
		}
		
		/// <summary>
		/// Start Config
		/// </summary>
		public void Start(int _x,int _y,int _minX,int _maxX,int _addX)
		{
			x=_x;
			y=_y;
			minX=_minX;
			maxX=_maxX;
			addX=_addX;
			playerLabel.Left=x;
			playerLabel.Top=y;
			playerLabel.Visible=true;
			isActive=true;
		}
		
		public void Freeze()
		{
			isActive=false;						
		}

		public void Defreeze()
		{
			isActive=true;						
		}
		
		public void Stop()
		{
			playerLabel.Visible=true;
			isActive=false;			
		}

		#region Move Entity
		public void MoveLeft()
		{
			if (!isActive)
			{
				return;
			}
			x-=addX;
			if (x<minX)
			{
				x=minX;
			}
			playerLabel.Left=x;

			playerLabel.Refresh();

		}
		
		public void MoveRight()
		{
			if (!isActive)
			{
				return;
			}
			x+=addX;
			if (x>maxX)
			{
				x=maxX;
			}
			playerLabel.Left=x;

			playerLabel.Refresh();
			
		}
		#endregion

		#region EntProp
		public int X {
			get { return x; }
		}

		public int Y {
			get { return y; }
		}

		public bool IsActive {
			get { return isActive; }
		}
		#endregion

	}
}
