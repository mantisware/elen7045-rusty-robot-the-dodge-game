
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Configuration;


namespace RainGameProject
{
	/// <summary>
	/// Description of MainForm.
	/// </summary>
	public partial class MainForm : Form
	{
	
		//TODO - Better for User input - less delay ;)
		//[DllImport("user32")]
		//static extern short GetAsyncKeyState(Keys vKey);

		private Player _player;
		private List<WaterDrop> _cloudWaterDrops;

		private Clouds _cloud;
		private int _playerXStart;
		private int _playerYStart;
		private int _cloudXStart;
		private int _cloudYStart;
		private int _points;

		private System.Random _random;
		private double _valueForCloudsWaterDrop;
		private bool _playerDie;
		private int _delayDie;
		private int _delaySequence;
		private int _lives;
		private int _pointsToGetNewLive;
		private int _cloudDelayValue;
		private bool _stillRunning=false;

		private float _benchMarkValue;
		private float _benchCheck;

        #region Config Variables
        string cnf_DropCount;
		string cnf_PlayerStartPos;
        string cnf_valueForCloudsWaterDrop;
        string cnf_cloudDelayValue;
        string cnf_pointsToGet;
        string cnf_pointsToTake;
        string cnf_livesToGet;
        string cnf_benchMarkValue;
        string cnf_dropDelayValue;
        #endregion

        /// <summary>
        /// Main form Init
        /// </summary>
        public MainForm()
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
			
			_random = new Random();

			_cloud = new Clouds(pn_CloudsContainer,pn_CloudsContainer2);

		}

		#region Load Configuration
        /// <summary>
        /// Load Configurations
        /// </summary>
		private void loadConfig()
		{
            //TODO - Validation checks for con-figs.

            this.cnf_DropCount = ConfigurationManager.AppSettings["cnf_DropCount"].ToString();
            this.cnf_PlayerStartPos = ConfigurationManager.AppSettings["cnf_PlayerStartPos"].ToString();
            this.cnf_valueForCloudsWaterDrop = ConfigurationManager.AppSettings["cnf_valueForCloudsWaterDrop"].ToString();
            this.cnf_cloudDelayValue = ConfigurationManager.AppSettings["cnf_cloudDelayValue"].ToString();
            this.cnf_pointsToGet = ConfigurationManager.AppSettings["cnf_pointsToGet"].ToString();
            this.cnf_pointsToTake = ConfigurationManager.AppSettings["cnf_pointsToTake"].ToString();
            this.cnf_livesToGet = ConfigurationManager.AppSettings["cnf_livesToGet"].ToString();
            this.cnf_benchMarkValue = ConfigurationManager.AppSettings["cnf_benchMarkValue"].ToString();
            this.cnf_dropDelayValue = ConfigurationManager.AppSettings["cnf_dropDelayValue"].ToString();

        }

        #endregion

        #region Movement

        /// <summary>
        /// Move the Player around, TODO - Make it smoother
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MainForm_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyCode == Keys.Left)
			{
				_player.MoveLeft(); // Move the Player Left
			}
			if (e.KeyCode == Keys.Right)
			{
				_player.MoveRight(); // Move the Player Right
			}
		}

		#endregion

		#region Main "Loop" ticker
		void Tm_GetKeysTick(object sender, EventArgs e)
		{
			if (this._playerDie)
			{
				// Check if the player has lives left
				if (_delayDie==0 && _delaySequence==0)
				{
					if ( _lives>0)
					{
						_playerDie=false;
						this.lb_Player.Visible=true;
						this.lb_PlayerDie1.Visible=false;
						_player.Defreeze();
						_cloud.Defreeze();
						return;
					}
					else
					{
						tm_GetKeys.Enabled=false;
						this.lb_GameOver.Visible=true;
					}
					
				}
				else
				{
					//Play Die Animation (Make it smoother)
					_delayDie-=1;
					if (_delayDie==0)
					{
						if (_delaySequence==3)
						{
							this.lb_PlayerDie1.Left=this.lb_Player.Left;
							this.lb_PlayerDie1.Top=this.lb_Player.Top-(this.lb_PlayerDie.Height-this.lb_Player.Height);
							this.lb_PlayerDie1.Visible=true;
							this.lb_PlayerDie.Visible=false;
							_delayDie=4;
							_delaySequence-=1;
						}
						else if (_delaySequence==2)
						{
							this.lb_PlayerDie2.Left=this.lb_Player.Left;
							this.lb_PlayerDie2.Top=this.lb_Player.Top-(this.lb_PlayerDie.Height-this.lb_Player.Height);
							this.lb_PlayerDie2.Visible=true;
							this.lb_PlayerDie1.Visible=false;
							_delayDie=4;
							_delaySequence-=1;
							
						}
						else if (_delaySequence==1)
						{
							this.lb_PlayerDie2.Visible=false;
							_delayDie=20;
							_delaySequence-=1;
							this.lb_Player.Visible=false;
							
						}
					}
				}
				return;
			}
			
		}

		#endregion

		/// <summary>
		/// Main Load
		/// </summary>
		void MainFormLoad(object sender, EventArgs e)
		{
            //Load Configuration Settings
            loadConfig();

            //Load Player
			_player=new Player(lb_Player);
			this._playerXStart=lb_Player.Left;
			this._playerYStart=lb_Player.Top;
			_player.Start(_playerXStart,_playerYStart,0,this.Width-lb_Player.Width,6);

            #region Create & load Clouds
            _cloudWaterDrops = new List<WaterDrop>();
			for (int dropCount = Convert.ToInt32(this.cnf_DropCount); dropCount >= 0; dropCount--)
			{
				_cloudWaterDrops.Add(new WaterDrop(this,dropCount));
			}

            //Set Clouds Start position
			this._cloudXStart=pn_CloudsContainer.Left;
			this._cloudYStart=pn_CloudsContainer.Top;

			_cloudDelayValue=Convert.ToInt32(this.cnf_cloudDelayValue);
            // Start
            _cloud.Start(_cloudXStart,_cloudYStart,-5,22,10,this.Width-pn_CloudsContainer.Width-40,0,this.Height-pn_CloudsContainer.Height,_cloudDelayValue);

            // Variable to get 'random' Y axes start for Drops.
            //_cloudYStart += 22;
            #endregion

            //Set Drop Delay
            this.tm_Movements.Interval = Convert.ToInt32(this.cnf_dropDelayValue);
            
			lb_DelayValue.Text=_cloud.Delay.ToString();

            //Points
			_points=0;
			lb_PointsValue.Text=_points.ToString();

            // Configure 'random' of water drops
            _valueForCloudsWaterDrop = (100 - Convert.ToInt32(this.cnf_valueForCloudsWaterDrop));
            _valueForCloudsWaterDrop = _valueForCloudsWaterDrop / 100;

            _playerDie =false;
			_lives=3;
			_pointsToGetNewLive=Convert.ToInt32(this.cnf_pointsToTake);

            //For Points Calc
            _benchCheck = 0;
            _benchMarkValue = Convert.ToInt32(this.cnf_benchMarkValue);

            //TODO - Implement shelter from Trees

        }
					
		/// <summary>
		/// Almost Like "Main" game loop - Works better than Thread with loop.
		/// </summary>
		void Tm_MovementsTick(object sender, EventArgs e)
		{
			if (_stillRunning)
			{
				return;
			}
			_benchCheck++;
			_stillRunning =true;
			_cloud.Move();
			
            // Stop Game - Game Over
			if (_cloud.Y1>=_player.Y)
			{
				_delayDie=6;
				_delaySequence=3;
				_benchMarkValue = -1;
				_playerDie =true;
				_lives=0;
				foreach(var drop in _cloudWaterDrops)
				{
					drop.Stop();
				}
				
				_player.Freeze();
				_cloud.Freeze();

				this.lb_PlayerDie.Left=this.lb_Player.Left;
				this.lb_PlayerDie.Top=this.lb_Player.Top-(this.lb_PlayerDie.Height-this.lb_Player.Height);
				this.lb_PlayerDie.Visible=true;
				_player.Stop();
				this.lb_LivesValue.Text=_lives.ToString();
				lb_GameOver.Text="! GAME OVER \n RUSTIC !";
				_stillRunning=false;
				return;

			}

			if (_benchCheck >= _benchMarkValue && !_playerDie)
			{
				_points += 10;
				_benchCheck = 0;
				lb_PointsValue.Text = _points.ToString();
			}
			else if(_playerDie)
			{
				//You Died... give me some points
				_points = _points - _pointsToGetNewLive;
				if(_points < 0)
				{
					_points = 0;
				}
			}

			// Is there a drop falling
			foreach (var _cloudWaterDrop in _cloudWaterDrops)
			{
                // Check if there is a drop
				if (_cloudWaterDrop.IsActive)
				{
					_cloudWaterDrop.Move();

					//Check if you Died
					if (_cloudWaterDrop.X >= _player.X && _cloudWaterDrop.X <= _player.X + this.lb_Player.Width && _cloudWaterDrop.Y >= _player.Y && _cloudWaterDrop.Y <= _player.Y + this.lb_Player.Height)
					{
						_delayDie = 4;
						_delaySequence = 3;
						_playerDie = true;
						_lives -= 1;
						_cloudWaterDrop.Stop();
						_player.Freeze();
						_cloud.Freeze();
						this.lb_PlayerDie.Left = this.lb_Player.Left;
						this.lb_PlayerDie.Top = this.lb_Player.Top - (this.lb_PlayerDie.Height - this.lb_Player.Height);
						this.lb_PlayerDie.Visible = true;
						_player.Stop();
						this.lb_LivesValue.Text = _lives.ToString();

					}
				}
				else if (_random.NextDouble() > _valueForCloudsWaterDrop)
				{
                    // Go with a new Drop
					_cloud.StartCloudsDrop(_cloudWaterDrop);
				}
			}

			
			_stillRunning=false;
		}
		
		/// <summary>
		/// When the game is over, restart.
		/// </summary>
		private void lb_GameOver_Click(object sender, EventArgs e)
		{
			Application.Restart();
		}
	}
}
