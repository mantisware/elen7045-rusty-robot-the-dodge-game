﻿
namespace RainGameProject
{
	partial class MainForm
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.tm_GetKeys = new System.Windows.Forms.Timer(this.components);
            this.lb_WaterDrop = new System.Windows.Forms.Label();
            this.tm_Movements = new System.Windows.Forms.Timer(this.components);
            this.lb_PointsText = new System.Windows.Forms.Label();
            this.lb_PointsValue = new System.Windows.Forms.Label();
            this.lb_DelayValue = new System.Windows.Forms.Label();
            this.lb_DelayText = new System.Windows.Forms.Label();
            this.lb_CloudsWaterDrop = new System.Windows.Forms.Label();
            this.lb_PlayerDie = new System.Windows.Forms.Label();
            this.lb_PlayerDie1 = new System.Windows.Forms.Label();
            this.lb_PlayerDie2 = new System.Windows.Forms.Label();
            this.lb_LivesValue = new System.Windows.Forms.Label();
            this.lb_LivesText = new System.Windows.Forms.Label();
            this.lb_GameOver = new System.Windows.Forms.Label();
            this.lb_CloudsWaterDrop1 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.pictureBox9 = new System.Windows.Forms.PictureBox();
            this.pictureBox10 = new System.Windows.Forms.PictureBox();
            this.pictureBox11 = new System.Windows.Forms.PictureBox();
            this.pictureBox12 = new System.Windows.Forms.PictureBox();
            this.pictureBox13 = new System.Windows.Forms.PictureBox();
            this.pictureBox14 = new System.Windows.Forms.PictureBox();
            this.pictureBox15 = new System.Windows.Forms.PictureBox();
            this.pn_CloudsContainer = new System.Windows.Forms.Panel();
            this.pn_CloudsContainer2 = new System.Windows.Forms.Panel();
            this.lb_Player = new System.Windows.Forms.Label();
            this.pictureBox16 = new System.Windows.Forms.PictureBox();
            this.pictureBox17 = new System.Windows.Forms.PictureBox();
            this.pictureBox18 = new System.Windows.Forms.PictureBox();
            this.pictureBox20 = new System.Windows.Forms.PictureBox();
            this.pictureBox21 = new System.Windows.Forms.PictureBox();
            this.pictureBox22 = new System.Windows.Forms.PictureBox();
            this.pictureBox23 = new System.Windows.Forms.PictureBox();
            this.pictureBox24 = new System.Windows.Forms.PictureBox();
            this.pictureBox25 = new System.Windows.Forms.PictureBox();
            this.pictureBox26 = new System.Windows.Forms.PictureBox();
            this.pictureBox19 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox19)).BeginInit();
            this.SuspendLayout();
            // 
            // tm_GetKeys
            // 
            this.tm_GetKeys.Enabled = true;
            this.tm_GetKeys.Interval = 50;
            this.tm_GetKeys.Tick += new System.EventHandler(this.Tm_GetKeysTick);
            // 
            // lb_WaterDrop
            // 
            this.lb_WaterDrop.BackColor = System.Drawing.Color.Transparent;
            this.lb_WaterDrop.Font = new System.Drawing.Font("Arial Black", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_WaterDrop.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.lb_WaterDrop.Location = new System.Drawing.Point(324, 383);
            this.lb_WaterDrop.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lb_WaterDrop.Name = "lb_WaterDrop";
            this.lb_WaterDrop.Size = new System.Drawing.Size(4, 12);
            this.lb_WaterDrop.TabIndex = 1;
            // 
            // tm_Movements
            // 
            this.tm_Movements.Enabled = true;
            this.tm_Movements.Interval = 30;
            this.tm_Movements.Tick += new System.EventHandler(this.Tm_MovementsTick);
            // 
            // lb_PointsText
            // 
            this.lb_PointsText.BackColor = System.Drawing.Color.Transparent;
            this.lb_PointsText.Font = new System.Drawing.Font("Arial Rounded MT Bold", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_PointsText.ForeColor = System.Drawing.Color.Green;
            this.lb_PointsText.Location = new System.Drawing.Point(4, 8);
            this.lb_PointsText.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lb_PointsText.Name = "lb_PointsText";
            this.lb_PointsText.Size = new System.Drawing.Size(68, 25);
            this.lb_PointsText.TabIndex = 4;
            this.lb_PointsText.Text = "Points:";
            // 
            // lb_PointsValue
            // 
            this.lb_PointsValue.BackColor = System.Drawing.Color.Transparent;
            this.lb_PointsValue.Font = new System.Drawing.Font("Arial Rounded MT Bold", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_PointsValue.ForeColor = System.Drawing.Color.Green;
            this.lb_PointsValue.Location = new System.Drawing.Point(66, 8);
            this.lb_PointsValue.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lb_PointsValue.Name = "lb_PointsValue";
            this.lb_PointsValue.Size = new System.Drawing.Size(68, 25);
            this.lb_PointsValue.TabIndex = 5;
            this.lb_PointsValue.Text = "0";
            this.lb_PointsValue.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lb_DelayValue
            // 
            this.lb_DelayValue.BackColor = System.Drawing.Color.Transparent;
            this.lb_DelayValue.Font = new System.Drawing.Font("Arial Rounded MT Bold", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_DelayValue.ForeColor = System.Drawing.Color.Green;
            this.lb_DelayValue.Location = new System.Drawing.Point(771, 8);
            this.lb_DelayValue.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lb_DelayValue.Name = "lb_DelayValue";
            this.lb_DelayValue.Size = new System.Drawing.Size(68, 25);
            this.lb_DelayValue.TabIndex = 7;
            this.lb_DelayValue.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.lb_DelayValue.Visible = false;
            // 
            // lb_DelayText
            // 
            this.lb_DelayText.BackColor = System.Drawing.Color.Transparent;
            this.lb_DelayText.Font = new System.Drawing.Font("Arial Rounded MT Bold", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_DelayText.ForeColor = System.Drawing.Color.Green;
            this.lb_DelayText.Location = new System.Drawing.Point(709, 8);
            this.lb_DelayText.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lb_DelayText.Name = "lb_DelayText";
            this.lb_DelayText.Size = new System.Drawing.Size(68, 25);
            this.lb_DelayText.TabIndex = 6;
            this.lb_DelayText.Text = "Delay:";
            this.lb_DelayText.Visible = false;
            // 
            // lb_CloudsWaterDrop
            // 
            this.lb_CloudsWaterDrop.BackColor = System.Drawing.Color.Transparent;
            this.lb_CloudsWaterDrop.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.lb_CloudsWaterDrop.Image = global::RainGameProject.Properties.Resources.tex_drop;
            this.lb_CloudsWaterDrop.Location = new System.Drawing.Point(1014, 30);
            this.lb_CloudsWaterDrop.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lb_CloudsWaterDrop.Name = "lb_CloudsWaterDrop";
            this.lb_CloudsWaterDrop.Size = new System.Drawing.Size(36, 70);
            this.lb_CloudsWaterDrop.TabIndex = 8;
            this.lb_CloudsWaterDrop.Visible = false;
            // 
            // lb_PlayerDie
            // 
            this.lb_PlayerDie.BackColor = System.Drawing.Color.Transparent;
            this.lb_PlayerDie.Image = global::RainGameProject.Properties.Resources.tex_die_1;
            this.lb_PlayerDie.Location = new System.Drawing.Point(1179, 8);
            this.lb_PlayerDie.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lb_PlayerDie.Name = "lb_PlayerDie";
            this.lb_PlayerDie.Size = new System.Drawing.Size(120, 111);
            this.lb_PlayerDie.TabIndex = 9;
            this.lb_PlayerDie.Visible = false;
            // 
            // lb_PlayerDie1
            // 
            this.lb_PlayerDie1.BackColor = System.Drawing.Color.Transparent;
            this.lb_PlayerDie1.Image = global::RainGameProject.Properties.Resources.tex_die_5;
            this.lb_PlayerDie1.Location = new System.Drawing.Point(1307, 8);
            this.lb_PlayerDie1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lb_PlayerDie1.Name = "lb_PlayerDie1";
            this.lb_PlayerDie1.Size = new System.Drawing.Size(120, 111);
            this.lb_PlayerDie1.TabIndex = 10;
            this.lb_PlayerDie1.Visible = false;
            // 
            // lb_PlayerDie2
            // 
            this.lb_PlayerDie2.BackColor = System.Drawing.Color.Transparent;
            this.lb_PlayerDie2.Image = global::RainGameProject.Properties.Resources.tex_die_10;
            this.lb_PlayerDie2.Location = new System.Drawing.Point(1435, 8);
            this.lb_PlayerDie2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lb_PlayerDie2.Name = "lb_PlayerDie2";
            this.lb_PlayerDie2.Size = new System.Drawing.Size(120, 111);
            this.lb_PlayerDie2.TabIndex = 11;
            this.lb_PlayerDie2.Visible = false;
            // 
            // lb_LivesValue
            // 
            this.lb_LivesValue.BackColor = System.Drawing.Color.Transparent;
            this.lb_LivesValue.Font = new System.Drawing.Font("Arial Rounded MT Bold", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_LivesValue.ForeColor = System.Drawing.Color.Green;
            this.lb_LivesValue.Location = new System.Drawing.Point(418, 8);
            this.lb_LivesValue.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lb_LivesValue.Name = "lb_LivesValue";
            this.lb_LivesValue.Size = new System.Drawing.Size(32, 25);
            this.lb_LivesValue.TabIndex = 13;
            this.lb_LivesValue.Text = "3";
            this.lb_LivesValue.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lb_LivesText
            // 
            this.lb_LivesText.BackColor = System.Drawing.Color.Transparent;
            this.lb_LivesText.Font = new System.Drawing.Font("Arial Rounded MT Bold", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_LivesText.ForeColor = System.Drawing.Color.Green;
            this.lb_LivesText.Location = new System.Drawing.Point(342, 8);
            this.lb_LivesText.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lb_LivesText.Name = "lb_LivesText";
            this.lb_LivesText.Size = new System.Drawing.Size(68, 25);
            this.lb_LivesText.TabIndex = 12;
            this.lb_LivesText.Text = "Lives:";
            // 
            // lb_GameOver
            // 
            this.lb_GameOver.BackColor = System.Drawing.Color.Transparent;
            this.lb_GameOver.Font = new System.Drawing.Font("Courier New", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_GameOver.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.lb_GameOver.Location = new System.Drawing.Point(565, 287);
            this.lb_GameOver.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lb_GameOver.Name = "lb_GameOver";
            this.lb_GameOver.Size = new System.Drawing.Size(432, 194);
            this.lb_GameOver.TabIndex = 16;
            this.lb_GameOver.Text = "GAME OVER\r\nRUSTIC !";
            this.lb_GameOver.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lb_GameOver.Visible = false;
            this.lb_GameOver.Click += new System.EventHandler(this.lb_GameOver_Click);
            // 
            // lb_CloudsWaterDrop1
            // 
            this.lb_CloudsWaterDrop1.BackColor = System.Drawing.Color.Transparent;
            this.lb_CloudsWaterDrop1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.lb_CloudsWaterDrop1.Image = global::RainGameProject.Properties.Resources.tex_drop;
            this.lb_CloudsWaterDrop1.Location = new System.Drawing.Point(1078, 36);
            this.lb_CloudsWaterDrop1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lb_CloudsWaterDrop1.Name = "lb_CloudsWaterDrop1";
            this.lb_CloudsWaterDrop1.Size = new System.Drawing.Size(36, 70);
            this.lb_CloudsWaterDrop1.TabIndex = 21;
            this.lb_CloudsWaterDrop1.Visible = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::RainGameProject.Properties.Resources.tex_ground;
            this.pictureBox1.InitialImage = global::RainGameProject.Properties.Resources.tex_ground;
            this.pictureBox1.Location = new System.Drawing.Point(996, 777);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(76, 76);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 22;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::RainGameProject.Properties.Resources.tex_ground;
            this.pictureBox2.InitialImage = global::RainGameProject.Properties.Resources.tex_ground;
            this.pictureBox2.Location = new System.Drawing.Point(921, 777);
            this.pictureBox2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(76, 76);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 23;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = global::RainGameProject.Properties.Resources.tex_ground;
            this.pictureBox3.InitialImage = global::RainGameProject.Properties.Resources.tex_ground;
            this.pictureBox3.Location = new System.Drawing.Point(846, 777);
            this.pictureBox3.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(76, 76);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 24;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = global::RainGameProject.Properties.Resources.tex_ground;
            this.pictureBox4.InitialImage = global::RainGameProject.Properties.Resources.tex_ground;
            this.pictureBox4.Location = new System.Drawing.Point(772, 777);
            this.pictureBox4.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(76, 76);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox4.TabIndex = 25;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBox5
            // 
            this.pictureBox5.Image = global::RainGameProject.Properties.Resources.tex_ground;
            this.pictureBox5.InitialImage = global::RainGameProject.Properties.Resources.tex_ground;
            this.pictureBox5.Location = new System.Drawing.Point(473, 777);
            this.pictureBox5.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(76, 76);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox5.TabIndex = 29;
            this.pictureBox5.TabStop = false;
            // 
            // pictureBox6
            // 
            this.pictureBox6.Image = global::RainGameProject.Properties.Resources.tex_ground;
            this.pictureBox6.InitialImage = global::RainGameProject.Properties.Resources.tex_ground;
            this.pictureBox6.Location = new System.Drawing.Point(547, 777);
            this.pictureBox6.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(76, 76);
            this.pictureBox6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox6.TabIndex = 28;
            this.pictureBox6.TabStop = false;
            // 
            // pictureBox7
            // 
            this.pictureBox7.Image = global::RainGameProject.Properties.Resources.tex_ground;
            this.pictureBox7.InitialImage = global::RainGameProject.Properties.Resources.tex_ground;
            this.pictureBox7.Location = new System.Drawing.Point(622, 777);
            this.pictureBox7.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(76, 76);
            this.pictureBox7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox7.TabIndex = 27;
            this.pictureBox7.TabStop = false;
            // 
            // pictureBox8
            // 
            this.pictureBox8.Image = global::RainGameProject.Properties.Resources.tex_ground;
            this.pictureBox8.InitialImage = global::RainGameProject.Properties.Resources.tex_ground;
            this.pictureBox8.Location = new System.Drawing.Point(697, 777);
            this.pictureBox8.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(76, 76);
            this.pictureBox8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox8.TabIndex = 26;
            this.pictureBox8.TabStop = false;
            // 
            // pictureBox9
            // 
            this.pictureBox9.Image = global::RainGameProject.Properties.Resources.tex_ground;
            this.pictureBox9.InitialImage = global::RainGameProject.Properties.Resources.tex_ground;
            this.pictureBox9.Location = new System.Drawing.Point(25, 777);
            this.pictureBox9.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pictureBox9.Name = "pictureBox9";
            this.pictureBox9.Size = new System.Drawing.Size(76, 76);
            this.pictureBox9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox9.TabIndex = 35;
            this.pictureBox9.TabStop = false;
            // 
            // pictureBox10
            // 
            this.pictureBox10.Image = global::RainGameProject.Properties.Resources.tex_ground;
            this.pictureBox10.InitialImage = global::RainGameProject.Properties.Resources.tex_ground;
            this.pictureBox10.Location = new System.Drawing.Point(100, 777);
            this.pictureBox10.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pictureBox10.Name = "pictureBox10";
            this.pictureBox10.Size = new System.Drawing.Size(76, 76);
            this.pictureBox10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox10.TabIndex = 34;
            this.pictureBox10.TabStop = false;
            // 
            // pictureBox11
            // 
            this.pictureBox11.Image = global::RainGameProject.Properties.Resources.tex_ground;
            this.pictureBox11.InitialImage = global::RainGameProject.Properties.Resources.tex_ground;
            this.pictureBox11.Location = new System.Drawing.Point(175, 777);
            this.pictureBox11.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pictureBox11.Name = "pictureBox11";
            this.pictureBox11.Size = new System.Drawing.Size(76, 76);
            this.pictureBox11.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox11.TabIndex = 33;
            this.pictureBox11.TabStop = false;
            // 
            // pictureBox12
            // 
            this.pictureBox12.Image = global::RainGameProject.Properties.Resources.tex_ground;
            this.pictureBox12.InitialImage = global::RainGameProject.Properties.Resources.tex_ground;
            this.pictureBox12.Location = new System.Drawing.Point(249, 777);
            this.pictureBox12.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pictureBox12.Name = "pictureBox12";
            this.pictureBox12.Size = new System.Drawing.Size(76, 76);
            this.pictureBox12.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox12.TabIndex = 32;
            this.pictureBox12.TabStop = false;
            // 
            // pictureBox13
            // 
            this.pictureBox13.Image = global::RainGameProject.Properties.Resources.tex_ground;
            this.pictureBox13.InitialImage = global::RainGameProject.Properties.Resources.tex_ground;
            this.pictureBox13.Location = new System.Drawing.Point(324, 777);
            this.pictureBox13.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pictureBox13.Name = "pictureBox13";
            this.pictureBox13.Size = new System.Drawing.Size(76, 76);
            this.pictureBox13.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox13.TabIndex = 31;
            this.pictureBox13.TabStop = false;
            // 
            // pictureBox14
            // 
            this.pictureBox14.Image = global::RainGameProject.Properties.Resources.tex_ground;
            this.pictureBox14.InitialImage = global::RainGameProject.Properties.Resources.tex_ground;
            this.pictureBox14.Location = new System.Drawing.Point(399, 777);
            this.pictureBox14.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pictureBox14.Name = "pictureBox14";
            this.pictureBox14.Size = new System.Drawing.Size(76, 76);
            this.pictureBox14.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox14.TabIndex = 30;
            this.pictureBox14.TabStop = false;
            // 
            // pictureBox15
            // 
            this.pictureBox15.Image = global::RainGameProject.Properties.Resources.tex_ground;
            this.pictureBox15.InitialImage = global::RainGameProject.Properties.Resources.tex_ground;
            this.pictureBox15.Location = new System.Drawing.Point(-8, 777);
            this.pictureBox15.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pictureBox15.Name = "pictureBox15";
            this.pictureBox15.Size = new System.Drawing.Size(76, 76);
            this.pictureBox15.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox15.TabIndex = 36;
            this.pictureBox15.TabStop = false;
            // 
            // pn_CloudsContainer
            // 
            this.pn_CloudsContainer.BackColor = System.Drawing.Color.Transparent;
            this.pn_CloudsContainer.Location = new System.Drawing.Point(12, 36);
            this.pn_CloudsContainer.Name = "pn_CloudsContainer";
            this.pn_CloudsContainer.Size = new System.Drawing.Size(128, 64);
            this.pn_CloudsContainer.TabIndex = 39;
            // 
            // pn_CloudsContainer2
            // 
            this.pn_CloudsContainer2.BackColor = System.Drawing.Color.Transparent;
            this.pn_CloudsContainer2.Location = new System.Drawing.Point(146, 36);
            this.pn_CloudsContainer2.Name = "pn_CloudsContainer2";
            this.pn_CloudsContainer2.Size = new System.Drawing.Size(128, 64);
            this.pn_CloudsContainer2.TabIndex = 40;
            // 
            // lb_Player
            // 
            this.lb_Player.BackColor = System.Drawing.Color.Transparent;
            this.lb_Player.Image = global::RainGameProject.Properties.Resources.tex_Idle_1;
            this.lb_Player.Location = new System.Drawing.Point(276, 670);
            this.lb_Player.Margin = new System.Windows.Forms.Padding(0);
            this.lb_Player.Name = "lb_Player";
            this.lb_Player.Size = new System.Drawing.Size(70, 111);
            this.lb_Player.TabIndex = 41;
            // 
            // pictureBox16
            // 
            this.pictureBox16.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox16.BackgroundImage = global::RainGameProject.Properties.Resources.tex_Bush_1;
            this.pictureBox16.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox16.Location = new System.Drawing.Point(1478, 727);
            this.pictureBox16.Name = "pictureBox16";
            this.pictureBox16.Size = new System.Drawing.Size(100, 50);
            this.pictureBox16.TabIndex = 42;
            this.pictureBox16.TabStop = false;
            // 
            // pictureBox17
            // 
            this.pictureBox17.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox17.BackgroundImage = global::RainGameProject.Properties.Resources.tex_Tree_2;
            this.pictureBox17.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox17.Location = new System.Drawing.Point(-4, 584);
            this.pictureBox17.Name = "pictureBox17";
            this.pictureBox17.Size = new System.Drawing.Size(180, 194);
            this.pictureBox17.TabIndex = 43;
            this.pictureBox17.TabStop = false;
            // 
            // pictureBox18
            // 
            this.pictureBox18.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox18.BackgroundImage = global::RainGameProject.Properties.Resources.tex_Stone;
            this.pictureBox18.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox18.Location = new System.Drawing.Point(1064, 737);
            this.pictureBox18.Name = "pictureBox18";
            this.pictureBox18.Size = new System.Drawing.Size(100, 50);
            this.pictureBox18.TabIndex = 44;
            this.pictureBox18.TabStop = false;
            // 
            // pictureBox20
            // 
            this.pictureBox20.Image = global::RainGameProject.Properties.Resources.tex_ground;
            this.pictureBox20.InitialImage = global::RainGameProject.Properties.Resources.tex_ground;
            this.pictureBox20.Location = new System.Drawing.Point(1064, 777);
            this.pictureBox20.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pictureBox20.Name = "pictureBox20";
            this.pictureBox20.Size = new System.Drawing.Size(76, 76);
            this.pictureBox20.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox20.TabIndex = 51;
            this.pictureBox20.TabStop = false;
            // 
            // pictureBox21
            // 
            this.pictureBox21.Image = global::RainGameProject.Properties.Resources.tex_ground;
            this.pictureBox21.InitialImage = global::RainGameProject.Properties.Resources.tex_ground;
            this.pictureBox21.Location = new System.Drawing.Point(1139, 777);
            this.pictureBox21.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pictureBox21.Name = "pictureBox21";
            this.pictureBox21.Size = new System.Drawing.Size(76, 76);
            this.pictureBox21.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox21.TabIndex = 50;
            this.pictureBox21.TabStop = false;
            // 
            // pictureBox22
            // 
            this.pictureBox22.Image = global::RainGameProject.Properties.Resources.tex_ground;
            this.pictureBox22.InitialImage = global::RainGameProject.Properties.Resources.tex_ground;
            this.pictureBox22.Location = new System.Drawing.Point(1214, 777);
            this.pictureBox22.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pictureBox22.Name = "pictureBox22";
            this.pictureBox22.Size = new System.Drawing.Size(76, 76);
            this.pictureBox22.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox22.TabIndex = 49;
            this.pictureBox22.TabStop = false;
            // 
            // pictureBox23
            // 
            this.pictureBox23.Image = global::RainGameProject.Properties.Resources.tex_ground;
            this.pictureBox23.InitialImage = global::RainGameProject.Properties.Resources.tex_ground;
            this.pictureBox23.Location = new System.Drawing.Point(1289, 777);
            this.pictureBox23.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pictureBox23.Name = "pictureBox23";
            this.pictureBox23.Size = new System.Drawing.Size(76, 76);
            this.pictureBox23.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox23.TabIndex = 48;
            this.pictureBox23.TabStop = false;
            // 
            // pictureBox24
            // 
            this.pictureBox24.Image = global::RainGameProject.Properties.Resources.tex_ground;
            this.pictureBox24.InitialImage = global::RainGameProject.Properties.Resources.tex_ground;
            this.pictureBox24.Location = new System.Drawing.Point(1363, 777);
            this.pictureBox24.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pictureBox24.Name = "pictureBox24";
            this.pictureBox24.Size = new System.Drawing.Size(76, 76);
            this.pictureBox24.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox24.TabIndex = 47;
            this.pictureBox24.TabStop = false;
            // 
            // pictureBox25
            // 
            this.pictureBox25.Image = global::RainGameProject.Properties.Resources.tex_ground;
            this.pictureBox25.InitialImage = global::RainGameProject.Properties.Resources.tex_ground;
            this.pictureBox25.Location = new System.Drawing.Point(1438, 777);
            this.pictureBox25.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pictureBox25.Name = "pictureBox25";
            this.pictureBox25.Size = new System.Drawing.Size(76, 76);
            this.pictureBox25.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox25.TabIndex = 46;
            this.pictureBox25.TabStop = false;
            // 
            // pictureBox26
            // 
            this.pictureBox26.Image = global::RainGameProject.Properties.Resources.tex_ground;
            this.pictureBox26.InitialImage = global::RainGameProject.Properties.Resources.tex_ground;
            this.pictureBox26.Location = new System.Drawing.Point(1513, 777);
            this.pictureBox26.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pictureBox26.Name = "pictureBox26";
            this.pictureBox26.Size = new System.Drawing.Size(76, 76);
            this.pictureBox26.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox26.TabIndex = 45;
            this.pictureBox26.TabStop = false;
            // 
            // pictureBox19
            // 
            this.pictureBox19.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox19.BackgroundImage = global::RainGameProject.Properties.Resources.tex_Tree_3;
            this.pictureBox19.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox19.Location = new System.Drawing.Point(1340, 584);
            this.pictureBox19.Name = "pictureBox19";
            this.pictureBox19.Size = new System.Drawing.Size(180, 194);
            this.pictureBox19.TabIndex = 52;
            this.pictureBox19.TabStop = false;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::RainGameProject.Properties.Resources.tex_bg;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1578, 844);
            this.Controls.Add(this.pictureBox20);
            this.Controls.Add(this.pictureBox21);
            this.Controls.Add(this.pictureBox22);
            this.Controls.Add(this.pictureBox23);
            this.Controls.Add(this.pictureBox24);
            this.Controls.Add(this.pictureBox25);
            this.Controls.Add(this.pictureBox26);
            this.Controls.Add(this.pictureBox15);
            this.Controls.Add(this.pictureBox9);
            this.Controls.Add(this.pictureBox10);
            this.Controls.Add(this.pictureBox11);
            this.Controls.Add(this.pictureBox12);
            this.Controls.Add(this.pictureBox13);
            this.Controls.Add(this.pictureBox14);
            this.Controls.Add(this.pictureBox5);
            this.Controls.Add(this.pictureBox6);
            this.Controls.Add(this.pictureBox7);
            this.Controls.Add(this.pictureBox8);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.pictureBox16);
            this.Controls.Add(this.pictureBox17);
            this.Controls.Add(this.pictureBox18);
            this.Controls.Add(this.pictureBox19);
            this.Controls.Add(this.pn_CloudsContainer2);
            this.Controls.Add(this.lb_GameOver);
            this.Controls.Add(this.lb_LivesValue);
            this.Controls.Add(this.lb_LivesText);
            this.Controls.Add(this.lb_PlayerDie2);
            this.Controls.Add(this.lb_PlayerDie1);
            this.Controls.Add(this.lb_PlayerDie);
            this.Controls.Add(this.lb_DelayValue);
            this.Controls.Add(this.lb_DelayText);
            this.Controls.Add(this.lb_PointsValue);
            this.Controls.Add(this.lb_PointsText);
            this.Controls.Add(this.lb_WaterDrop);
            this.Controls.Add(this.pn_CloudsContainer);
            this.Controls.Add(this.lb_Player);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.MainFormLoad);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MainForm_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox19)).EndInit();
            this.ResumeLayout(false);

		}
		private System.Windows.Forms.Label lb_CloudsWaterDrop1;
		private System.Windows.Forms.Label lb_GameOver;
		private System.Windows.Forms.Label lb_LivesText;
		private System.Windows.Forms.Label lb_LivesValue;
		private System.Windows.Forms.Label lb_PointsText;
		private System.Windows.Forms.Label lb_PlayerDie2;
		private System.Windows.Forms.Label lb_PlayerDie1;
		private System.Windows.Forms.Label lb_PlayerDie;
		private System.Windows.Forms.Label lb_CloudsWaterDrop;
		private System.Windows.Forms.Label lb_DelayText;
		private System.Windows.Forms.Label lb_DelayValue;
		private System.Windows.Forms.Label lb_PointsValue;
		private System.Windows.Forms.Timer tm_Movements;
		private System.Windows.Forms.Label lb_WaterDrop;
		private System.Windows.Forms.Timer tm_GetKeys;
		private System.Windows.Forms.PictureBox pictureBox1;
		private System.Windows.Forms.PictureBox pictureBox2;
		private System.Windows.Forms.PictureBox pictureBox3;
		private System.Windows.Forms.PictureBox pictureBox4;
		private System.Windows.Forms.PictureBox pictureBox5;
		private System.Windows.Forms.PictureBox pictureBox6;
		private System.Windows.Forms.PictureBox pictureBox7;
		private System.Windows.Forms.PictureBox pictureBox8;
		private System.Windows.Forms.PictureBox pictureBox9;
		private System.Windows.Forms.PictureBox pictureBox10;
		private System.Windows.Forms.PictureBox pictureBox11;
		private System.Windows.Forms.PictureBox pictureBox12;
		private System.Windows.Forms.PictureBox pictureBox13;
		private System.Windows.Forms.PictureBox pictureBox14;
		private System.Windows.Forms.PictureBox pictureBox15;
		private System.Windows.Forms.Panel pn_CloudsContainer;
		private System.Windows.Forms.Panel pn_CloudsContainer2;
		private System.Windows.Forms.Label lb_Player;
		private System.Windows.Forms.PictureBox pictureBox16;
		private System.Windows.Forms.PictureBox pictureBox17;
		private System.Windows.Forms.PictureBox pictureBox18;
		private System.Windows.Forms.PictureBox pictureBox20;
		private System.Windows.Forms.PictureBox pictureBox21;
		private System.Windows.Forms.PictureBox pictureBox22;
		private System.Windows.Forms.PictureBox pictureBox23;
		private System.Windows.Forms.PictureBox pictureBox24;
		private System.Windows.Forms.PictureBox pictureBox25;
		private System.Windows.Forms.PictureBox pictureBox26;
		private System.Windows.Forms.PictureBox pictureBox19;
	}
}
